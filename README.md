# doc
my docs

```
.
├── cs
│   ├── algorithm
│   │   ├── way-to-algorithm.pdf
│   │   ├── 刷题总结.pdf
│   │   ├── 十五个经典算法研究与总结.pdf
│   │   ├── 柔性字符串匹配.pdf
│   │   ├── 程序员编程艺术.pdf
│   │   ├── 程序员面试逻辑题解析.pdf
│   │   ├── 程序员面试金典 第5版.pdf
│   │   └── 编程之美.pdf
│   ├── c&cpp
│   │   ├── C++_Primer_Plus_勘误(第6版).pdf
│   │   ├── C++ Primer Plus  第6版.pdf
│   │   ├── C专家编程.pdf
│   │   ├── C++标准程序库.pdf
│   │   ├── C语言接口与实现--创建可重用软件的技术.pdf
│   │   ├── Deep C.pdf
│   │   ├── google_c++编程风格(高清版).pdf
│   │   ├── More_Effective_C++(Recommondate).pdf
│   │   ├── STL源码剖析.pdf
│   │   ├── 你必须知道的495个C语言问题.pdf
│   │   └── 深度探索C++对象模型.pdf
│   ├── C C++中国象棋程序入门与提高.pdf
│   ├── compiler
│   │   ├── llvm-implementing-a-language.pdf
│   │   ├── 正则表达式.doc
│   │   ├── 词法分析.doc
│   │   └── 陈意云ppt
│   │       ├── chapter10.ppt
│   │       ├── chapter11.ppt
│   │       ├── chapter12.ppt
│   │       ├── chapter13.ppt
│   │       ├── chapter1.ppt
│   │       ├── chapter2.ppt
│   │       ├── chapter3.ppt
│   │       ├── chapter4.ppt
│   │       ├── chapter5.ppt
│   │       ├── chapter6.ppt
│   │       ├── chapter7.ppt
│   │       ├── chapter8.ppt
│   │       └── chapter9.ppt
│   ├── distributed
│   │   ├── blockchain_guide.pdf
│   │   └── 分布式系统原理 介绍.pdf
│   ├── docker
│   │   ├── docker_practice.pdf
│   │   └── 第一本DOCKER书 修订版.pdf
│   ├── Go入门指南.pdf
│   ├── linux
│   │   ├── Linux From Scratch.pdf
│   │   ├── UNIX环境高级编程 第二版.pdf
│   │   ├── 深入理解linux内核 第三版.pdf
│   │   └── 深入理解LINUX网络技术内幕.pdf
│   ├── network
│   │   ├── H3C网络大爬虫
│   │   │   ├── 交换专题.pdf
│   │   │   ├── 网络大爬虫第3期-BGP专题.pdf
│   │   │   └── 网络大爬虫第6期-MPLS专题.pdf
│   │   ├── IP基础技术
│   │   │   ├── IP基础技术_01_数通基础PPT
│   │   │   │   ├── 01_数通基础_OSI网际互联.pptx
│   │   │   │   ├── 02_TCPIP概述、IP子网划分（VLSM）.pptx
│   │   │   │   ├── 03_网络设备管理及基础配置.pptx
│   │   │   │   ├── 04_网络拓扑绘制技能专题.pptx
│   │   │   │   └── 05_eNSP华为网络设备仿真平台.pptx
│   │   │   ├── IP基础技术_02_交换基础PPT
│   │   │   │   ├── 06_以太网二层交换基础.pptx
│   │   │   │   ├── 07_STP生成树协议基础.pptx
│   │   │   │   ├── 08_实现VLAN间的通信.pptx
│   │   │   │   ├── 09_以太网端口镜像（含报文分析）和链路聚合.pptx
│   │   │   │   └── 10_Smart_Link_&_Monitor_Link.pptx
│   │   │   ├── IP基础技术_03_路由基础PPT
│   │   │   │   ├── 11_IP路由选择原理、静态路由、路由汇总的概念.pptx
│   │   │   │   ├── 12_IP路由选择原理（提高篇）.pptx
│   │   │   │   ├── 13_动态路由协议、RIP.pptx
│   │   │   │   ├── 14_OSPF基础.pptx
│   │   │   │   ├── 15_路由重发布基础专题.pptx
│   │   │   │   ├── 16_路由策略技术专题.pptx
│   │   │   │   └── 17_OSPF_LSA及特殊区域详解.pdf
│   │   │   ├── IP基础技术_04_BGP基础PPT
│   │   │   │   ├── 18_BGP概述及基础配置.pdf
│   │   │   │   ├── 19_VRRP技术专题.pdf
│   │   │   │   ├── 20_BGP路径属性详解.pdf
│   │   │   │   ├── 21_BGP路由策略技术专题.pdf
│   │   │   │   ├── 22_BGP路由反射器及联邦.pptx
│   │   │   │   └── 23_BGP路由优选规则详解.pdf
│   │   │   ├── IP基础技术_05_组播基础PPT
│   │   │   │   ├── 24_IP组播概述及IGMP.pdf
│   │   │   │   └── 25_组播路由协议PIM.pdf
│   │   │   └── IP基础技术_06_安全基础PPT
│   │   │       ├── 26_ACL访问控制列表基础.pptx
│   │   │       ├── 27_NAT网络地址转换.pdf
│   │   │       ├── 28_VPN基础.pptx
│   │   │       └── 29_VPN实例（VRF）的概念及简单应用.pptx
│   │   ├── MPLS.ppt
│   │   ├── network-basic.pdf
│   │   ├── SDN
│   │   │   ├── sdn.pdf
│   │   │   ├── 深度解析SDN：利益、战略、技术、实践.pdf
│   │   │   ├── 腾云：云计算和大数据时代网络技术揭秘.pdf
│   │   │   └── 重构网络：SDN架构实现.pdf
│   │   ├── TCP-IP详解
│   │   │   ├── tcp_ip_illustrated_volume_1_the_protocols_2nd_edit.pdf
│   │   │   ├── TCP-IP详解卷一：协议.pdf
│   │   │   ├── TCP-IP详解卷三：TCP事务协议，HTTP，NNTP和UNIX域协议.pdf
│   │   │   └── TCP-IP详解卷二：实现.pdf
│   │   ├── VPN.pdf
│   │   ├── 云计算数据中心网络技术.pdf
│   │   ├── 构建中小企业网络H3CNE V6.0培训教材
│   │   │   ├── X00010001 第1章 计算机网络概述.ppt
│   │   │   ├── X00010002 第2章 OSI参考模型与TCP IP模型.ppt
│   │   │   ├── X00010003 第3章 局域网基本原理.ppt
│   │   │   ├── X00010004 第4章 广域网基本原理.ppt
│   │   │   ├── X00010005 第5章 IP基本原理.ppt
│   │   │   ├── X00010006 第6章 TCP和UDP基本原理.ppt
│   │   │   ├── X00020001 第7章 路由器、交换机及其操作系统介绍.ppt
│   │   │   ├── X00020002 第8章 命令行操作基础.ppt
│   │   │   ├── X00020003 第9章 网络设备文件管理.ppt
│   │   │   ├── X00020004 第10章 网络设备基本调试.ppt
│   │   │   ├── X00030001 第11章 以太网交换机工作原理.ppt
│   │   │   ├── X00030002 第12章 配置VLAN.ppt
│   │   │   ├── X00030003 第13章 生成树协议.ppt
│   │   │   ├── X00030004 第14章 交换机端口安全技术.ppt
│   │   │   ├── X00030005 第15章 配置链路聚合.ppt
│   │   │   ├── X00040001 第16章 IP子网划分.ppt
│   │   │   ├── X00040003 第17章 DNS.ppt
│   │   │   ├── X00040004 第18章 文件传输协议.ppt
│   │   │   ├── X00040005 第19章 DHCP.ppt
│   │   │   ├── X00040100 第20章 IPv6基础.ppt
│   │   │   ├── X00050001 第21章 IP路由原理.ppt
│   │   │   ├── X00050002 第22章 直连路由和静态路由.ppt
│   │   │   ├── X00050003 第23章 路由协议概述.ppt
│   │   │   ├── X00050004 第24章 RIP原理.ppt
│   │   │   ├── X00050005 第25章 配置RIP.ppt
│   │   │   ├── X00050101 第26章 OSPF基础.ppt
│   │   │   ├── X00060100 第27章 用访问控制列表实现包过滤.ppt
│   │   │   ├── X00060201 第28章 网络地址转换.ppt
│   │   │   ├── X00070002 第29章 配置HDLC.ppt
│   │   │   ├── X00070003 第30章 配置PPP.ppt
│   │   │   ├── X00070004 第31章 配置帧中继.ppt
│   │   │   ├── X00070005 第32章 ISDN和DCC.ppt
│   │   │   └── 构建中小企业网络v6.0_常用图标说明0101.ppt
│   │   ├── 路由交换
│   │   │   ├── H3C网络学院 路由交换第一卷（上册）.pdf
│   │   │   └── H3C网络学院 路由交换第一卷（下册）.pdf
│   │   └── 路由协议
│   │       ├── DA000009 IS-IS协议 ISSUE 1.0.docx
│   │       ├── DA000010 OSPF路由协议 ISSUE 1.0.doc
│   │       └── DA000011 BGP路由协议 ISSUE 1.0.docx
│   ├── nginx开发从入门到精通.pdf
│   ├── re-for-beginners.pdf
│   ├── security
│   │   ├── Linux环境下常见漏洞利用技术
│   │   │   ├── examples.rar
│   │   │   └── Linux环境下常见漏洞利用技术.pdf
│   │   └── 安全客
│   │       ├── security-geek-2016-A.pdf
│   │       ├── security-geek-2016-B.pdf
│   │       ├── security-geek-2017-q1.pdf
│   │       ├── security-geek-2017-q2.pdf
│   │       └── security-geek-2017-q3.pdf
│   ├── tool
│   │   ├── 100-gdb-tips.pdf
│   │   ├── cmake实践.pdf
│   │   ├── DOT guide.pdf
│   │   ├── progit2.pdf
│   │   ├── valgrind-memcheck.pdf
│   │   └── vim_cheat_sheet_for_programmers_print.pdf
│   ├── windows
│   │   ├── VC++深入详解.pdf
│   │   ├── Windows 核心编程 第5版.pdf
│   │   └── 深入浅出MFC 第2版.pdf
│   ├── 微服务：从设计到部署.pdf
│   ├── 汇编语言.pdf
│   ├── 程序员的自我修养.pdf
│   ├── 经典密码学与现代密码学.pdf
│   ├── 计算机和难解性 NP完全性理论导引.pdf
│   └── 设计模式：可复用面向对象软件的基础.pdf
├── English
│   ├── Barack Obama - 20110302 - Remarks on Death of Osama bin Laden
│   │   ├── address.mp3
│   │   └── script.pdf
│   ├── Common Sense.pdf
│   ├── Declaration of Independence.pdf
│   ├── Donald Trump - 20190218 - Remarks to the Venezuelan American Community
│   │   ├── address.m4a
│   │   └── script.pdf
│   ├── George Bush - 20010920 - Joint Session Address on Terrorist Attacks
│   │   ├── address.mp3
│   │   └── script.pdf
│   ├── Israeli Decalaration of Independence.pdf
│   ├── Jimmy Carter - 19750715 - A Crisis of Confidence
│   │   ├── address.mp3
│   │   └── script.pdf
│   ├── John Kennedy - 19600120 - Presidential Inaugural Address
│   │   ├── address.mp3
│   │   └── script.pdf
│   ├── John Kennedy - 19630626 - Ich bin ein Berliner (I am a 'Berliner')
│   │   ├── address.mp3
│   │   └── script.pdf
│   ├── Mike Pence - 20181004 - Remarks on the Administration’s Policy Toward China
│   │   ├── address.m4a
│   │   └── script.pdf
│   ├── Mike Pence - 20191024 - Remarks by Vice President Pence at the Frederic V. Malek Memorial Lecture
│   │   ├── address.mp3
│   │   └── script.pdf
│   ├── Ronald Reagan - 19641027 - A Time for Choosing
│   │   ├── address.mp3
│   │   └── script.pdf
│   ├── Ronald Reagan - 19830323 - Remarks at the Annual Convention of the the National Association of Evangelicals
│   │   ├── address.mp3
│   │   └── script.pdf
│   ├── Ronald Reagan - 19840430 - Remarks at Fudan University in Shanghai
│   │   ├── address.mp3
│   │   └── script.pdf
│   ├── Ronald Reagan - 19870612 - Remarks at the Brandenburg Gate
│   │   ├── address.mp3
│   │   └── script.pdf
│   ├── Ronald Reagan - 19890111 - Farewell Address to the Nation
│   │   ├── address.mp3
│   │   └── script.pdf
│   ├── The Mayflower Compact.pdf
│   ├── Tiananmen Square 1989 II.pdf
│   ├── Tiananmen Square 1989 I.pdf
│   └── Walden.pdf
├── math
│   ├── 具体数学.pdf
│   ├── 同济线性代数.pdf
│   ├── 思考的乐趣：Matrix67数学笔记.pdf
│   ├── 概率统计讲义.pdf
│   ├── 离散数学及其应用  原书第7版.pdf
│   ├── 离散数学教程 屈婉玲 耿素云 王捍贫 V2.pdf
│   └── 魔方
│       ├── CFOP
│       │   ├── CROSS - 十字天书.doc
│       │   ├── F2L.png
│       │   ├── OLL.jpg
│       │   └── PLL.jpg
│       └── 魔方原理及其应用.pdf
├── misc
│   ├── 中国国家治理的制度逻辑.pdf
│   ├── 中国官僚体制中的非正式制度.pdf
│   ├── 中国经济是怎么被玩垮的.pdf
│   ├── 以色列：一个民族的重生.pdf
│   ├── 你不知道的美国那些事儿.pdf
│   ├── 圣经故事.pdf
│   ├── 大国大城.pdf
│   ├── 孙子兵法.pdf
│   ├── 庐山会议实录.pdf
│   ├── 当代中国政府过程.pdf
│   ├── 愤怒的人民已不再恐惧.pdf
│   ├── 我们当下的恐惧与期待.pdf
│   ├── 李锐口述往事.pdf
│   ├── 格言联璧.pdf
│   ├── 永久记录.pdf
│   ├── 红顶商人胡雪岩.pdf
│   └── 老子.pdf
└── README.md

45 directories, 213 files
```
